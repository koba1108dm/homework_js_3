// let age = prompt("Будь ласка, введіть свій вік:");

//         if (age < 12) {
//             alert("Ви є дитиною.");
//         } else if (age < 18) {
//             alert("Ви є підлітком.");
//         } else {
//             alert("Ви є дорослим.");

//         }


        let month = prompt("Введіть місяць");

        let days;
        switch (month) {
            case "січень":
            case "березень":
            case "травень":
            case "липень":
            case "серпень":
            case "жовтень":
            case "грудень":
                days = 31;
                break;
            case "квітень":
            case "червень":
            case "вересень":
            case "листопад":
                days = 30;
                break;
            case "лютий":
                days = 28;
                break;
            default:
                console.log("Введено некоректний місяць.");
                days = null;
                break;
        }

        if (days !== null) {
            console.log(`Місяць ${month} має ${days} днів.`);
        }